function [] = plotBeamPath(results)
% plotBeamPath -
%
% Syntax: [in_data] = plotBeamPath(drx, dry, dz, theta, )
%
% Inputs:
%    - drx, dry, dz, theta,  -
%
% Outputs:
%    - in_data -

plot3(100*[results.p1(1), results.p2(1)],100*[results.p1(2), results.p2(2)], 100*[results.p1(3), results.p2(3)], 'linewidth', 0.5)
plot3(100*[results.p2(1), results.p3(1)],100*[results.p2(2), results.p3(2)], 100*[results.p2(3), results.p3(3)], 'linewidth', 0.5)
plot3(100*[results.p3(1), results.p4(1)],100*[results.p3(2), results.p4(2)], 100*[results.p3(3), results.p4(3)], 'linewidth', 0.5)
