function [] = plotCrystals(results, args)
% plotCrystals -
%
% Syntax: [in_data] = plotCrystals(drx, dry, dz, theta, )
%
% Inputs:
%    - drx, dry, dz, theta,  -
%
% Outputs:
%    - in_data -

arguments
    results
    args.color (3,1) double {mustBeNumeric} = [1;1;1]
    args.alpha (1,1) double {mustBeNumeric} = 1
end

z = results.np;
y = [0,1,0];
x = cross(y,z);
xtal1_rectangle = [results.pp + 0.02/2*y + 0.035/2*x;
                   results.pp - 0.02/2*y + 0.035/2*x;
                   results.pp - 0.02/2*y - 0.035/2*x;
                   results.pp + 0.02/2*y - 0.035/2*x];

patch(100*xtal1_rectangle(:,1), 100*xtal1_rectangle(:,2), 100*xtal1_rectangle(:,3), 'k-')

z = results.ns;
% y = [0,cos(drx),sin(drx)];
y = [0,1,0];
x = cross(y,z);
xtal2_rectangle = [results.ps + 0.02/2*y + 0.07/2*x;
                   results.ps - 0.02/2*y + 0.07/2*x;
                   results.ps - 0.02/2*y - 0.07/2*x;
                   results.ps + 0.02/2*y - 0.07/2*x];

patch(100*xtal2_rectangle(:,1), 100*xtal2_rectangle(:,2), 100*xtal2_rectangle(:,3), 'k-')
